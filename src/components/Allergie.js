import React, { useState } from "react";
import Card from "./UI/Card2";
import classes from "./Allergie.module.css";
import Button from "./UI/Button";
import ErrorModal from "./UI/ErrorModal";

const Allergie = (props) => {
  const [enteredScore, setEnteredscore] = useState("");
  const [error, setError] = useState("");
  const AddUserHandlerScore = (event) => {
    event.preventDefault();
    if (+enteredScore < 1) {
      setError({
        title: "Invalid score",
        message: "Please entered a valid number not a text",
      });
      return;
    }
    if (+enteredScore > 10) {
      setError({
        title: "He is an allergic person",
        message: " His allergies are peanuts,shellfish,strawberries,tomatoes",
      });
      setEnteredscore("");
      return;
    }
    if (+enteredScore === 1) {
      setError({
        title: "He is an allergic person",
        message: "Allergic to eggs",
      });
      setEnteredscore("");
      return;
    }
    props.onAddUser({ score: enteredScore });
    setEnteredscore("");
  };

  const scoreChangeHandler = (event) => {
    setEnteredscore(event.target.value);
  };

  const errorHandler = () => {
    setError(null);
  };

  return (
    <>
      {error && (
        <ErrorModal
          title={error.title}
          message={error.message}
          onConfirm={errorHandler}
        />
      )}
      <Card className={classes.input}>
        <form>
          <label htmlFor="score">What are your allergic score?</label>
          <input
            id="score"
            type="number"
            value={enteredScore}
            onChange={scoreChangeHandler}
            required
          />
          <Button onClick={AddUserHandlerScore} type="Submit">
            Add score
          </Button>
        </form>
      </Card>
    </>
  );
};

export default Allergie;
