import React from "react";
import classes from "./Card.module.css";

const Card = (props) => {
  return (
    <div className={` ${classes.card} ${props.className}`}>
      Congragdulation you are not not an allergic person.
      {props.children}
    </div>
  );
};

export default Card;
