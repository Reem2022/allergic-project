import React, { useState } from "react";
import Allergie from "./components/Allergie";
import UserList from "./components/UserList";

function App(props) {
  const [userList, setUserList] = useState([]);
  const addUserHandler = ({ score }) => {
    setUserList((prevUsersList) => {
      return [...prevUsersList, { score, id: Math.random().toString() }];
    });
  };
  return (
    <>
      <Allergie onAddUser={addUserHandler} />
      <UserList users={userList} />
    </>
  );
}

export default App;

// The component should have an input field that asks the user for their allergy score and a button that will submit the users answer.
// The input should accept only  numbers and throw an html error if the score is not a number.
// Then the component will use yesterday's functions to calculate if the user is allergic and if they are it will print the list of allergies to the screen.
